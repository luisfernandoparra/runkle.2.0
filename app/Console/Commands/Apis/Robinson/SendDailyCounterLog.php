<?php

namespace App\Console\Commands\Apis\Robinson;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use GuzzleHttp\Client;

class SendDailyCounterLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:robinson:senddailycounterlog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron o command que  manda un log del número de checks que se han realizado, y de qué tipo.';

    /**
     * Variable que dirá desde hace cuántos días que extraemos los datos
     *
     * @var integer
     */
    protected $previousDays = 5;

    /**
     * Variable que dirá desde hace cuántos meses sacamos el histórico de checks realizados en total
     *
     * @var integer
     */
    protected $previousMonths = 3;


    /**
     * Estilos a insertar en el HTML
     *
     * @var string
     */
    protected $htmlStyles = "  <style>
    table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }
    
    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }
    
    tr:nth-child(even) {
      background-color: #dddddd;
    }

    th {
        background-color: #f2f2f2;
    }
    </style>";

    /**
     * Estructura principal de la tabla
     *
     * @var string
     */
    protected $htmlTableDefinition = "<table><thead><tr>{{ header }}</tr></thead><tbody>{{ body }}</tbody></table>";

    protected $htmlEmail = '<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
        <title>e-retail data&technology</title>
        {{ styles }}
        </head>
        <html><body>
        <hr>
<h3>Total according to ADigital WS</h3>
<h4>{{ totalByADigital }}*</h4><small>* ADigital does not offers the actual data</small>
<br><br><hr>
<h3>Checks last {{ previousDays }} Days</h3>
{{ tableByDays }}<br><br><hr>
<h3>Checks last {{ previousMonths }} Months</h3>
{{ tableByMonths }}
        </body></html>
        ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Función que conecta con ADigital y recibe el número de checks realizados
     *
     * @return integer
     */
    private function getTotalChequed()
    {
        $url = config('apis.robinson.urlCounter');
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url, ['http_errors' => false]);
        $statusCode = $response->getStatusCode();



        $resp = (string) $response->getBody();
        $respJson = json_decode($resp);
        if (json_last_error() !== JSON_ERROR_NONE)
            return 0;
        else {
            if ($respJson->error != 0 || $respJson->result != 'OK') {
                return 0;
            } else {
                return $respJson->counter;
            }
        }
        /**
         * Si llegamos aquí es casi imposible. Pero pa porsi devolvemos algo
         */
        return 0;
    }

    /**
     * Función que envía el email al WS de ws-server
     *
     * @param string $html
     * @return void
     */
    private function sendEmail($html)
    {
        $url = config('misc.mail.url');

        $arr = [
            'user' => 'SES-WSSEVER2', 'data' =>
            [
                [
                    'receiver' => 'lfparra@e-retailadvertising.com',
                    'subject' => 'ADigital Log ' . Carbon::now()->format('Y-m-d'),
                    'html' => $html,
                    'text' => 'Text de envío'
                ]
            ]
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->post($url, ['json' => $arr, 'http_errors' => false]);
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {





        /**
         * variable para la cabecera que contendrá las fechas
         */
        $htmlTableTotalHeader = "";
        /**
         * Variable para la cabecera del resumen total por mes
         */
        $htmlTableResumeByMonthHeader = "";
        /**
         * Para simplificar y no hacer un inner join, vamos a sacar primero los tipos de datos que existen, y posteriormente los datos en sí
         */
        $objChecktypes = DB::connection('api_robinson')->table('check_types')->select('id', 'check_type', 'check_type_internal')->get();
        $arrCheckTypes = [];
        foreach ($objChecktypes as $key => $value) {
            $arrCheckTypes[$value->id] = trim($value->check_type);
            $htmlTableTotalHeader .= "<th>" . trim($value->check_type) . "</th>";
            $htmlTableResumeByMonthHeader .= "<th>" . trim($value->check_type) . "</th>";
        }
        $htmlTableTotalHeader .= "<th>Email +</th><th>Telephone +</th>";
        $htmlTableResumeByMonthHeader .= "<th>Email +</th>";
        $htmlTableResumeByMonthHeader .= "<th>Telephone +</th>";

        $htmlTableResumeByMonthHeader .= "<th>Total</th>";



/**
 * Sacamos datos mensuales
 */
        $dateMonth = Carbon::now()->subMonths($this->previousMonths)->format('Y-m');
        $objTotalData = DB::connection('api_robinson')->table('check_historical')->select("check_type_id", DB::raw("count(*) as counter"), DB::raw("DATE_FORMAT(created_at,'%Y-%m') as date"))->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"), ">=", $dateMonth)->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"), "check_type_id")->orderBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m'), check_type_id"), "asc")->get();
        $objTotalDataEmail = DB::connection('api_robinson')->table('email_checks')->select(DB::raw("count(*) as counter"), DB::raw("DATE_FORMAT(created_at,'%Y-%m') as date"))->where([[DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"), ">=", $dateMonth], ['in_robinson', 1]])->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"))->orderBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"), "asc")->get();
        $objTotalDataTelephone = DB::connection('api_robinson')->table('telephone_checks')->select(DB::raw("count(*) as counter"), DB::raw("DATE_FORMAT(created_at,'%Y-%m') as date"))->where([[DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"), ">=", $dateMonth], ['in_robinson', 1]])->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"))->orderBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"), "asc")->get();
        
        $arrTotalByMonth = [];
        $arrTotalEmailByMonth =  [];
        $arrTotalTelephoneByMonth = [];
        foreach ($objTotalData as $key => $value) {

            $typeId = $value->check_type_id;
            $counter = $value->counter;

            $dateMonthString = Carbon::createFromFormat('Y-m', $value->date)->format('F Y');
            if (!array_key_exists($dateMonthString, $arrTotalByMonth)) {
                $arrTotalByMonth[$dateMonthString] = [];
            }
            $arrTotalByMonth[$dateMonthString][$typeId] = $counter;
        }

        foreach ($objTotalDataEmail as $key => $value) {
            $dateMonthString = Carbon::createFromFormat('Y-m', $value->date)->format('F Y');

            $arrTotalEmailByMonth[$dateMonthString] = $value->counter;
        }
        foreach ($objTotalDataTelephone as $key => $value) {
            $dateMonthString = Carbon::createFromFormat('Y-m', $value->date)->format('F Y');

            $arrTotalTelephoneByMonth[$dateMonthString] = $value->counter;
        }
        $htmlTableTotalByMonthBody = "";
        foreach ($arrTotalByMonth as $date => $arrValues) {
            $total = 0;

            $htmlTableTotalByMonthBody  .= "<tr>";
            $htmlTableTotalByMonthBody  .= "<td>$date</td>";
            foreach ($arrCheckTypes as $checkTypeId => $nothinga) {
                if (!empty($arrValues[$checkTypeId])) {
                    $htmlTableTotalByMonthBody .= "<td>" . $arrValues[$checkTypeId] . "</td>";
                    $total = $total + (int) $arrValues[$checkTypeId];
                } else {
                    $htmlTableTotalByMonthBody .= "<td>0</td>";
                }
            }
            $htmlTableTotalByMonthBody .= "<td>".(array_key_exists($date,$arrTotalEmailByMonth) ? $arrTotalEmailByMonth[$date] : '0')."</td>";
            $htmlTableTotalByMonthBody .= "<td>".(array_key_exists($date,$arrTotalTelephoneByMonth) ? $arrTotalTelephoneByMonth[$date] : '0')."</td>";
            $htmlTableTotalByMonthBody .= "<td><span style='font-weight:bold'>$total</span></td>";

            $htmlTableTotalByMonthBody  .= "</tr>";
        }
        $htmlTableTotalByMonth = str_replace(["{{ header }}", '{{ body }}'], ["<th>Date</th>" . $htmlTableResumeByMonthHeader, $htmlTableTotalByMonthBody], $this->htmlTableDefinition);





        $date = Carbon::now()->subDays($this->previousDays)->format('Y-m-d');

        /**
         * Sacamos los datos de email y teléfono que se han procesado como de Lista Robinson
         */
        /** Email */
        $objEmailInRobinson = DB::connection('api_robinson')->table('email_checks')->select(DB::raw("count(*) as counter"), DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as date"))->where([[DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"), ">=", $date], ['in_robinson', 1]])->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"))->orderBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"), "asc")->get();
        $arrEmailInRobinson = [];
        foreach ($objEmailInRobinson as $key => $value) {

            $arrEmailInRobinson[$value->date] = $value->counter;
        }
        /** Teléfonos */
        $objTelephoneInRobinson = DB::connection('api_robinson')->table('telephone_checks')->select(DB::raw("count(*) as counter"), DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as date"))->where([[DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"), ">=", $date], ['in_robinson', 1]])->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"))->orderBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"), "asc")->get();
        $arrTelephoneInRobinson = [];
        foreach ($objTelephoneInRobinson as $key => $value) {

            $arrTelephoneInRobinson[$value->date] = $value->counter;
        }
        /**
         * Sacamos los datos totales
         */
        $objTotalData = DB::connection('api_robinson')->table('check_historical')->select("check_type_id", DB::raw("count(*) as counter"), DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as date"))->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"), ">=", $date)->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"), "check_type_id")->orderBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d'), check_type_id"), "asc")->get();




        $arrTmpData = [];

        foreach ($objTotalData as $key => $value) {
            $date = $value->date;
            $counter = $value->counter;
            $check_type_id = $value->check_type_id;
            if (!array_key_exists($date, $arrTmpData)) {
                $arrTmpData[$date] = [];
            }
            $arrTmpData[$date][$check_type_id] = $counter;
        }
        $htmlTableTotalBody = "";

        foreach ($arrTmpData as $date => $arrValues) {
            $htmlTableTotalBody .= "<tr>";
            $htmlTableTotalBody .= "<td>$date</td>";
            foreach ($arrCheckTypes as $checkTypeId => $nothinga) {
                if (!empty($arrValues[$checkTypeId])) {
                    $htmlTableTotalBody .= "<td>" . $arrValues[$checkTypeId] . "</td>";
                } else {
                    $htmlTableTotalBody .= "<td>0</td>";
                }
            }
            if (!empty($arrEmailInRobinson[$date])) {
                $htmlTableTotalBody .= "<td>" . $arrEmailInRobinson[$date] . "</td>";
            } else {
                $htmlTableTotalBody .= "<td>0</td>";
            }
            if (!empty($arrTelephoneInRobinson[$date])) {
                $htmlTableTotalBody .= "<td>" . $arrTelephoneInRobinson[$date] . "</td>";
            } else {
                $htmlTableTotalBody .= "<td>0</td>";
            }

            $htmlTableTotalBody .= "</tr>";
        }
        $htmlTableTotal = str_replace(["{{ header }}", '{{ body }}'], ["<th>Date</th>" . $htmlTableTotalHeader, $htmlTableTotalBody], $this->htmlTableDefinition);



        /**
         * Totales
         */
        $totalByADigital = $this->getTotalChequed();

        $emailBody = str_replace(
            ['{{ styles }}', '{{ totalByADigital }}', '{{ previousDays }}', '{{ tableByDays }}', '{{ previousMonths }}', '{{ tableByMonths }}'],
            [$this->htmlStyles, $totalByADigital, $this->previousDays, $htmlTableTotal, $this->previousMonths, $htmlTableTotalByMonth],
            $this->htmlEmail
        );



        $this->sendEmail($emailBody);

        //dd($emailBody);
    }
}
