<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecksTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('api_robinson')->create('check_types', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('check_type')->comment('Type of Check to perform');
            $table->string('check_type_internal')->comment('Internal Type of Check (for internal use)');
            $table->string('prefix_robinson',2)->comment('Prefix needed for performing the check at Adigital');
            
            $table->comment = "Table that gets the type of checks";
        });
        DB::connection('api_robinson')->table('check_types')->insert([
            ['check_type'=>'Email','check_type_internal'=>'EMAIL','prefix_robinson'=>'02'],
            ['check_type'=>'Telephone SMS','check_type_internal'=>'TELEPHONE-SMS','prefix_robinson'=>'03'],
            ['check_type'=>'Telephone Calls','check_type_internal'=>'TELEPHONE-CALL','prefix_robinson'=>'04']

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('api_robinson')->dropIfExists('check_types');
    }
}
