<?php

/**
 * @date 2020.04.06
 * Añadimos funcionalidad check Broken LInks
 */

namespace App\Http\Controllers\esp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GlLinkChecker\GlLinkChecker;
use GuzzleHttp\Client;

use Symfony\Component\Finder\Finder;
//use GuzzleHttp\Client;




class EspController extends Controller
{
    /**
     * Funcionalidad que de un html devuelve plain text
     *
     * @param Request $request
     * @return void
     */
    public function espHtmlToText(Request $request)
    {
        $validation = $request->validate([
            'datainput' => 'required'
        ]);
        $options = ["ignore_errors" => true];
        $text = \Soundasleep\Html2Text::convert($request->datainput, $options);
        return ['response' => $text];
        // dd("Texto Resultante ".$text);
    }

    /**
     * Función que utilizará la librería https://github.com/emmanuelroecker/php-linkchecker lkjlhjklkjkljugjuggfhfjfghgdfghfghghgfhggjhkhhjbhjhjhgjghgjghhfdgfhghjdfhghgjeryuyghjghjhgj4rrtrtfvb ertyuiodfghjhngjhgfh654654tyr
     *
     * @param Request $request
     * @return void
     */
    public function brokenLinksCheckerGlicer(Request $request) {
        $linkChecker  = new GlLinkChecker('http://lyon.glicer.com');
        dd($linkChecker);
    }

    public function brokenLinksCheckerGuzzle(Request $request) {
        $validation = $request->validate([
            'url' => 'required|url'
        ]);
        $link = $request->url;
        $link = str_replace(array('&amp;'),array('&'),$link);
        $allHttps = true;
        $output = array(
            'allHttps' => true,
            'correctLink' => false,
            'host'=>'',
            'data' => array()
        );
        $lastLink ="";


        

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $link, ['debug' => true, 
        //'http_errors' => false,
        'allow_redirects' => [
            'max'             => 10,        // allow at most 10 redirects.
            'strict'          => true,      // use "strict" RFC compliant redirects.
            'referer'         => true,      // add a Referer header
            'protocols'       => ['https'], // only allow https URLs
           // 'on_redirect'     => $onRedirect,
            'track_redirects' => true
        ],
        'timeout'=>0,'connect_timeout'=>0]);
        $this->http_status = $statusCode = $response->getStatusCode();
        $header = [];
        foreach ($response->getHeaders() as $name => $values) {
            $header[] = $name . ": " . implode(", ", $values);
        }
        dd($response->getHeaders());
    }

    /**
     * Funcionalidad que dado un link devuelve si es un broken link y si todos los saltos son servidor seguro
     * no usamos Guzzle porque no devolvía información que necesitábamos. 
     *
     * @param Request $request
     * @return void
     */
    public function brokenLinksChecker(Request $request)
    {
        $validation = $request->validate([
            'url' => 'required|url'
        ]);
        $link = $request->url;
        $allHttps = true;
        $output = array(
            'allHttps' => true,
            'correctLink' => false,
            'host'=>'',
            'data' => array()
        );
        $lastLink ="";




     

        $ch = curl_init();
       // curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');


        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $headers = curl_getinfo($ch);
        $lastLink   = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);

        $httpPort = (int) $headers['primary_port'];
        $httpCode = $headers['http_code'];

        $output['correctLink'] = (int) ($httpCode == '200');
        $output['allHttps'] = (int) ($httpPort == 443);
        $output['host'] = parse_url($lastLink, PHP_URL_SCHEME) . "://" . parse_url($lastLink, PHP_URL_HOST);
        $output['url_final'] = $lastLink;
        $output['jumps'] = $headers['redirect_count'];
        $output['return'] = $headers;
       



/*

        do {
           // $link = str_replace('&amp;','&',$link);
            $log = array(
                'url' => $link
            );
            
            $lastLink = $link;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $link);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch , CURLOPT_RETURNTRANSFER, 1);
            
           // curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
          //  curl_setopt($ch,CURLOPT_SSL_VERIFYSTATUS,true);
            $data = curl_exec($ch);
            $headers = curl_getinfo($ch);
            $link = $headers['redirect_url'];
            $httpPort = (int)$headers['primary_port'];
            $httpCode = $headers['http_code'];
            $allHttps = $allHttps && ($httpPort == 443);
          
            
            $httpPort = (int) $headers['primary_port'];
            $httpCode = $headers['http_code'];
            $allHttps = $output['allHttps'] && ($httpPort == 443);
            $link = $headers['redirect_url'];
            $log['httpResponse'] = $httpCode;
            $log['port'] = $httpPort;
            $log['portSecure'] = ($httpPort==443);
            $log['linkNext'] = $link;

            $log['data']=$headers;
            $output['data'][] = $log;
            

            unset($log);
        } while ($link != '');
        $output['correctLink'] = (int)($httpCode=='200');
        $output['allHttps'] = (int)$allHttps;  
        $output['host'] =parse_url($lastLink, PHP_URL_SCHEME)."://". parse_url($lastLink, PHP_URL_HOST);
        $output['lastUrl'] = $lastLink;
        $output['jumps'] = count($output['data']);
       */

        return $output;
       // die(json_encode($output));
    }
}
