<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelephoneChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('api_robinson')->create('telephone_checks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedTinyInteger('status_id')->default(1)->comment('FK to status. Status of the Item');
            $table->string('telephone')->comment('Telephone to be checked');
            $table->boolean('in_robinson')->default(0)->comment('If the Telephone is at Robinson List. either call or sms 1: Is at Robinson. 0: not listed');
            $table->boolean('in_robinson_call')->default(0)->comment('CALLs: If the Telephone is at Robinson List. 1: Is at Robinson. 0: not listed');
            $table->boolean('in_robinson_sms')->default(0)->comment('SMS: If the Telephone is at Robinson List. 1: Is at Robinson. 0: not listed');

            $table->integer('response_http_status_call')->default(0)->comment('Http Status returned by ADigital');
            $table->integer('response_http_status_sms')->default(0)->comment('Http Status returned by ADigital');

            $table->json('response_full_call')->nullable()->comment('Full reponse from ADigital');
            $table->json('response_full_sms')->nullable()->comment('Full reponse from ADigital');

            $table->ipAddress('ip_source')->nullable()->comment('Ip address that create the query');
            $table->timestamp('created_at')->useCurrent()->comment('Fecha del Chequeo');
            $table->timestamp('checked_call_at')->nullable()->comment('For background checks, date it was checked');
            $table->timestamp('checked_sms_at')->nullable()->comment('For background checks, date it was checked');

            $table->timestamp('retrieved_at')->nullable()->comment('For background checks, date it was retrieved by external system');
            $table->unique('telephone');
            $table->index('status_id');
            $table->index('in_robinson');
            $table->index('in_robinson_call');
            $table->index('in_robinson_sms');

            

        });
        Schema::connection('api_robinson')->table('telephone_checks', function($table) {
            $table->foreign('status_id')->references('id')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('api_robinson')->dropIfExists('telephone_checks');
    }
}
