<?php

use Swagger\Annotations as OA;




/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="WS-SERVER2",
 *      description="e-retail data Api",
 *      @OA\Contact(
 *          email="lfparra@e-retailadvertising.com"
 *      ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 */
/**
 *  @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Api Production Server"
 *  )
 *
 *  @OA\Server(
*      url="http://runkle.2.0.test/api",
 *      description="Api Development Server"
 * )
 */
/**
 * @OA\SecurityScheme(
 *     type="oauth2",
 *     description="Use a global client_id / client_secret and your username / password combo to obtain a token",
 *     name="Password Based",
 *     in="header",
 *     scheme="https",
 *     securityScheme="Password Based",
 *     @OA\Flow(
 *         flow="password",
 *         authorizationUrl="/oauth/authorize",
 *         tokenUrl="/oauth/token",
 *         refreshUrl="/oauth/token/refresh",
 *         scopes={}
 *     )
 * )
 */
/**
 * @OA\Tag(
 *     name="esp",
 *     description="Api for ESP matters"
 * )
 *
 * @OA\ExternalDocumentation(
 *     description="Find out more about how implement in your side this api",
 *     url="https://dev.e-retaildata.com/stat-server/swagger/manual/"
 * )
 */


/**
 * @OA\Post(
 *      path = "/esp/htmltotext",
 *      operationId="htmltotext",
 *      tags={"esp"},
 *      summary="Convert Html to Plain Text",
 *      description="A HTML is sent via Post and a Plain Text for being used in email campaigns is returned. Production Server: https://ws-server2.e-retaildata.com/api/esp/htmltotext",
 * @OA\Parameter(
 *          name="Accept",
 *          description="Accept application/json",
 *          required=true,
 *          in="header",
 *          example="application/json",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                  required={"datainput"},
 *                 @OA\Property(
 *                     property="datainput",
 *                      description="Html to be modified",
 *                     type="string"
 *                 ),
 *                 example={"datainput": "<span>This is a simple example</span>"}
 *             )
 *         )
 *     ),
 *      @OA\Response(
 *          response=200,
 *          description="successful operation that includes the Plain HTML",
 *          @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                  required={"response"},
 *                 @OA\Property(
 *                     property="response",
 *                      description="Result of the modification. Plain Text to be sent",
 *                     type="string"
 *                 ),
 *                 example={"response": "This is a simple example"}
 *             )
 *         )
 *              
 * 
 *          
 *       )
 *      
 * )
 */




/**
 * @OA\Get(
 *      path="/projects",
 *      operationId="getProjectsList",
 *      tags={"Projects"},
 *      summary="Get list of projects",
 *      description="Returns list of projects",
 *      @OA\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @OA\Response(response=400, description="Bad request"),
 *       security={
 *           {"api_key_security_example": {}}
 *       }
 *     )
 *
 * Returns list of projects
 */

/**
 * @OA\Get(
 *      path="/robinson-list/email/{email}",
 *      operationId="checkRobinsonInputEmail",
 *      tags={"Robinson List"},
 *      summary="Check if Email is in Robinson List",
 *      description="Returns list of projects",
 *      @OA\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *       @OA\Response(response=400, description="Bad request"),
 *       security={
 *           {"api_key_security_example": {}}
 *       }
 *     )
 *
 * Returns list of projects
 */






/**
 * @OA\Get(
 *      path="/projects/{id}",
 *      operationId="getProjectById",
 *      tags={"Projects"},
 *      summary="Get project information",
 *      description="Returns project data",
 *      @OA\Parameter(
 *          name="id",
 *          description="Project id",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 *      security={
 *         {
 *             "oauth2_security_example": {"write:projects", "read:projects"}
 *         }
 *     },
 * )
 */