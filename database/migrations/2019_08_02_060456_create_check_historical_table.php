<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckHistoricalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('api_robinson')->create('check_historical', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedTinyInteger('check_type_id')->comment('Type of check. FK to check_types');
            $table->unsignedBigInteger('item_id')->comment('Virtual FK to email_checks or telephone_checks');
            $table->timestamp('created_at')->useCurrent();

            $table->index(['check_type_id', 'item_id']);
            $table->index('check_type_id');
        });
        Schema::connection('api_robinson')->table('check_historical', function($table) {
            $table->foreign('check_type_id')->references('id')->on('check_types');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('api_robinson')->dropIfExists('check_historical');
    }
}
