<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('api_robinson')->create('status', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('status')->comment('Status of the Item');
        });
        DB::connection('api_robinson')->table('status')->insert([
            ['status'=>'Pending Of Processing'],
            ['status'=>'Processed and Retrieved'],
            ['status'=>'Semaphore'],
            ['status'=>'Pending Of Retrieved'],
            ['status'=>'Retrieved'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('api_robinson')->dropIfExists('status');
    }
}
