<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
//Route::get('data/data-bases/list/{status?}','SegmentationController@getDataBaseList')->where("status", "^(|all)$")/*->middleware(['auth:api', 'scopes:SuperAdmin,DataManagement'])*/;


Route::post('esp/htmltotext','esp\EspController@espHtmlToText');

Route::post('esp/broken-links-checker','esp\EspController@brokenLinksChecker');
Route::post('esp/broken-links-checker-guzzle','esp\EspController@brokenLinksCheckerGuzzle');

//Route::post('esp/broken-links-checker-glicer','esp\EspController@brokenLinksCheckerGlicer');


//Route::get('robinson-list/{function}/{item}','Apis\RobinsonListController@checkInput2')->where("function","(emails|telephone)");

Route::get('robinson-list/email/{email}','Apis\RobinsonListController@checkInputEmail');//->where("input","/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/");
Route::get('robinson-list/telephone/{telephone}','Apis\RobinsonListController@checkInputTelephone');//->where("telephone","^(\+34|0034|34)?[6|7|8|9][0-9]{8}$");
Route::get('robinson-list/request-count','Apis\RobinsonListController@checkNumRequest');


