<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('api_robinson')->create('email_checks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedTinyInteger('status_id')->default(1)->comment('FK to status. Status of the Item');
            $table->string('email')->comment('Email to be checked');
            $table->boolean('in_robinson')->default(0)->comment('If the email is at Robinson List. 1: Is at Robinson. 0: not listed');
            $table->integer('response_http_status')->default(0)->comment('Http Status returned by ADigital');
            $table->json('response_full')->nullable()->comment('Full reponse from ADigital');
            $table->ipAddress('ip_source')->nullable()->comment('Ip address that create the query');
            $table->timestamp('created_at')->useCurrent()->comment('Fecha del Chequeo');
            $table->timestamp('checked_at')->nullable()->comment('For background checks, date it was checked');
            $table->timestamp('retrieved_at')->nullable()->comment('For background checks, date it was retrieved by external system');
            $table->unique('email');
            $table->index('status_id');
            $table->index('in_robinson');
            

        });
        Schema::connection('api_robinson')->table('email_checks', function($table) {
            $table->foreign('status_id')->references('id')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('api_robinson')->dropIfExists('email_checks');
    }
}
