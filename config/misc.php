<?php

return [

    /**
     * Configurador de variables miscelaneas, por ejemplo, para conecdtar con el sistema que manda mail
     */

    'mail' => [
        'url'=>'https://ws-server.e-retaildata.com/webservices/transactionalEmails/index.php'
    ],
];