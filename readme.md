<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Summary

## About WS-SERVER2 Api

Servidor de Microservicios desarrollado por e-retail data technology para su Ecosistema de Sistemas.
Versión inicial: 2019.03.29

Es una actualización del existente pero desarrollado en:

- PHP 7
- Laravel Framework
- ApiREST 
- Nginx
- MySql (no utilizada aún)

## Before First Deployment... Previous Steps

Se definen variables del entorno y Bases de Datos que hay que crear. En el caso de las bases de datos, hay que crearlas y definirlas ANTES de ejecutar el paso 7:

```bash
php artisan migrate
```

La definición de las BBDD a crear se encuentran en el fichero `.env` de raiz del proyecto.

- `DB_DATABASE` Base de datos principal común para todas las apps. Se crearán tablas de control.  
- `DB_DATABASE_API_ROBINSON` Base de datos para la Api que chequea la Lista Robinson

```bash
CREATE DATABASE IF NOT EXISTS `runkle2` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE DATABASE IF NOT EXISTS `runkle2_robinson` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
```

## First Deploy
1. Setup deploy ssh keys at gitlab project configuration.
2. [Install composer](https://getcomposer.org/download/) and [Setup globally](https://getcomposer.org/doc/00-intro.md#globally)
3. `git clone git@gitlab.com:e-retail-data/runkle.2.0.git runkle.2.0`
4. `cd runkle.2.0`
5. `cp .env.example .env` and setup .env with correct values.
6. `composer install --no-dev`
7. `php artisan migrate` will populate database
8.  `php artisan config:cache`
9.  `php artisan route:cache`


## Update & deploy
```bash
cd /path-to-your-project/
```
As we have the project deployed as nginx we need to switch to nginx user (this user also has the deploy key configured at git server)

```bash
sudo -Hu nginx bash
```
Now we are at proyect root path and we are the right user

```bash
php artisan cache:clear && php artisan route:clear && php artisan config:clear
```

```bash
git pull origin master && composer install --no-dev && php artisan migrate &&  php artisan config:cache && php artisan route:cache
```

# Api Definitions

Puedes encontrar todas las definiciones de las Apis en este sitio web [http://ws-server.e-retaildata.com/swagger/](e-retail technology Api Definitions)

## HtmltoText

Api desarrollado para el ESP Enver donde dado un HTML (total o parcial) devuelve el Plain Text del email para ser enviado.

Puedes ver la propia definición en el entorno de definición de Apis [e-retail technology Api Definitions](http://ws-server.e-retaildata.com/swagger/) o directamente en la definición del servidor: [Swagger Api Definition](https://ws-server2.e-retaildata.com/api/documentation)

Utiliza la librería [soundasleep/html2text](https://github.com/soundasleep/html2text). La utilizamos para uso interno no comercial.

- Acceso: **libre**
- Url: https://ws-server2.e-retaildata.com/api/esp/htmltotext
- Header:
  - Content: application/json
  - Accept: application/json
- Definition: [Swagger Api Definition](https://ws-server2.e-retaildata.com/api/documentation) o [e-retail technology Api Definitions](http://ws-server.e-retaildata.com/swagger/)

## Robinson Check

Api desarrollada para el Ecosistema de e-retail. Conecta con el chequeo de Emails o Teléfonos en la Lista Robinson gestionada por ADigital. 

[NOT DEVELOPED YET] Puedes ver la propia definición en el entorno de definición de Apis [e-retail technology Api Definitions](http://ws-server.e-retaildata.com/swagger/) o directamente en la definición del servidor: [Swagger Api Definition](https://ws-server2.e-retaildata.com/api/documentation)

- Acceso: **Restringido**


## Broken Links Checker

Api desarrollada para el ESP Enver, donde dado un Link devuelve un array con información de dicho link: saltos, si es broken link, si todos los saltos son en servidor seguro, URL final donde llega...

Dado que Google cada vez está convirtiendo los links no seguros en Broken Links necesitamos un sistema que dado un dominio, chequee no solo a donde llega sino si todos los saltos hasta llegar a la url final son todos saltos en servidores seguros. 

- Acceso: **libre**
- Url: https://ws-server2.e-retaildata.com/api/esp/broken-links-checker
- Header:
  - Content: application/json
  - Accept: application/json
- Definition: [Swagger Api Definition](https://ws-server2.e-retaildata.com/api/documentation) o [e-retail technology Api Definitions](http://ws-server.e-retaildata.com/swagger/)

# License

Copyright by e-retail data Technology 2019 [e-retail](http://www.e-retailadvertising.com/).

About using the Api, we are using 3rd party development. Thanks to:
- soundasleep/html2text https://github.com/soundasleep/html2text
