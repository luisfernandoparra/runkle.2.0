<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('application')->comment('Name of the application');
            $table->text('definition')->nullable()->comment('Description of the App');
            $table->string('definition_url')->nullable()->comment('Swagger or any other information about Api/App definition');
            $table->string('local_route')->nullable()->comment('Local route of the app');
            $table->boolean('active')->default(true)->comment('If the application is active');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

        });
        DB::table('apps')->insert([
            ['application'=>'Robinson List','definition'=>'Api que chequea si un email o un teléfono están en la Lista Robinson, conectando con la Api de ADigital','local_route'=>'/api/robinson-list/',
            'created_at'=>Carbon::now()->format('Y-m-d H:i:s')]
        ]);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apps');
        
        
    }
}
