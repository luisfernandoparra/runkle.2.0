<?php

namespace App\Http\Requests\Apis;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

use Route;

class RobinsonTelephoneValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'telephone'=>['required','regex:/^(\+34|0034|34)?[6|7|8|9][0-9]{8}$/','numeric'],
            'ip'=>['required','ip',Rule::exists('apps_clients_access_ips')->where(function ($query) {
                $query->where([['app_id', 1],['active',1]]);
            }),
        ],
        ];
    }

    protected function validationData()
    {
        return array_merge($this->request->all(), [
            'telephone' => Route::input('telephone'),
            'ip'=>FormRequest::ip(),
        ]);
    }
}
