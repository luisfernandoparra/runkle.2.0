<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdatedAtTelephone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('api_robinson')->table('telephone_checks', function (Blueprint $table) {
            $table->timestamp('lastaccess_at')->nullable()->comment('Whenever a system checked such item');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('api_robinson')->table('telephone_checks', function (Blueprint $table) {
            $table->dropColumn(['lastaccess_at']);
        });
    }
}
