<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppsClientsAccessIpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps_clients_access_ips', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('app_id')->unsigned()->comment('FK to apps');
            $table->bigInteger('client_id')->unsigned()->comment('FK to clients');
            $table->ipAddress('ip')->comment('Access Ip');
            $table->boolean('active')->default(true)->comment('if the ip is active');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->foreign('app_id')->references('id')->on('apps');
            $table->foreign('client_id')->references('id')->on('clients');

            $table->comment = 'Ip Allowed by Client and App';
        });
        DB::table('apps_clients_access_ips')->insert([
            ['app_id'=>1,'client_id'=>1,'ip'=>'176.58.125.192'],
            ['app_id'=>1,'client_id'=>2,'ip'=>'85.159.213.228'],
            ['app_id'=>1,'client_id'=>3,'ip'=>'136.162.246.12'],
            ['app_id'=>1,'client_id'=>4,'ip'=>'212.71.235.216'],
            ['app_id'=>1,'client_id'=>5,'ip'=>'62.97.126.66'],
        ]);
        if (env('APP_ENV')=='local') {
            DB::table('apps_clients_access_ips')->insert([
                ['app_id'=>1,'client_id'=>6,'ip'=>'127.0.0.1'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apps_clients_access_ips');
    }
}
