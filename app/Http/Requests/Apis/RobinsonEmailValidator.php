<?php

namespace App\Http\Requests\Apis;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Route;

class RobinsonEmailValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

       /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//Rule::in(['10.0.0.1'])

        return [
            'email' => ['required','email'],
            'ip'=>['required','ip',Rule::exists('apps_clients_access_ips')->where(function ($query) {
                $query->where([['app_id', 1],['active',1]]);
            }),
        ],
        ];
    }


    protected function validationData()
    {
        return array_merge($this->request->all(), [
            'email' => Route::input('email'),
            'ip'=>FormRequest::ip(),
        ]);
    }
}
