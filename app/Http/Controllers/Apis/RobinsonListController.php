<?php

/**
 * Modificamos la función checkHistoricChecksEmail y la de teléfonos para actualizar la última vez que hemos chequeado. Si no rechequeamos, actualizamos la BBDD 
 */

namespace App\Http\Controllers\Apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Apis\RobinsonEmailValidator;
use App\Http\Requests\Apis\RobinsonTelephoneValidator;
use GuzzleHttp\Client;
use function GuzzleHttp\json_decode;
use Carbon\Carbon;
use DB;

class RobinsonListController extends Controller
{
    /**
     * Tipo de algoritmo. su valor está en config->apis.roginson.algorithm
     *
     * @var string
     */
    private $algorithm;

    /**
     * Tipo de Check a realizar. 
     * Values: CHECKINLIST, REQUESTCOUNT
     *
     * @var string
     */
    private $checkType = 'CHECKINLIST';

    /**
     * Variable que dice si se está en modo debug
     *
     * @var boolean
     */
    private $___debug = false;

    /**
     * Variable que dice si aún no estando en omodo debug, depura las llamadas y datos
     *
     * @var boolean
     */
    private $___showDebug = false;
    /**
     * Variable que inserta el http status devuelto en la llamada
     *
     * @var integer
     */
    private $http_status = 0;

    /**
     * Variable que inserta el http status devuelto en la llamada
     *
     * @var integer
     */
    private $http_status_sms = 0;

    /**
     * Variable que se introduce con la respuesta completa de la Api
     *
     * @var json
     */
    private $http_full_response = '';
    /**
     * Variable que se introduce con la respuesta completa de la Api (segundo chequeo para teléfono)
     *
     * @var json
     */
    private $http_full_response_sms = '';

    /**
     * Ip del usuario que ha accedido
     *
     * @var string
     */
    private $ip = '';

    /**
     * Código HTTP Status de chequeo en la lista Robinson
     *
     * @var array
     */
    protected $httpsStatusResponseList = [
        'INROBINSON' => 200,
        'NOTROBINSON' => 404,
        'ERROR' => 211,
        'NOTPROCESSED' => 422
    ];

    /**
     * Array que contiene los tipos de checks. 
     * Es la información de la tabla check_types
     *
     * @var array
     */
    protected $check_types =  [
        'EMAIL' => ['id' => 1, 'cod' => '02'],
        'TELEPHONE-SMS' => ['id' => 2, 'cod' => '03'],
        'TELEPHONE-CALL' => ['id' => 3, 'cod' => '04']
    ];

    /**
     * Variable que dice el chequeo que se eestá haciendo en cada momento
     *
     * @var string
     */
    private $check_type_current = 'EMAIL';


    function ____debug($object)
    {
        if ($this->___debug || $this->___showDebug)
            dump($object);
    }

    /**
     * Función que contacta con la lista robinson para chequear el núnero de requests generadas
     * Lo utilizaremos de testeo de conexión
     *
     * @param Request $request
     * @return void
     */
    function checkNumRequest(Request $request)
    {
        $this->algorithm = config('apis.robinson.algorithm');
        $this->checkType = 'REQUESTCOUNT';
        $headers = $this->getRequestHeader();
        $res = $this->sendRequest('', $headers);
        $responseOut = ['error' => 1, 'result' => 'KO', 'msg' => 'Request cannot be processed right now', 'status' => $this->http_status, 'counter' => 0];
        $numRequests = 0;
        $result = 'KO';

        if ($this->http_status == 200) {
            //Hemos recibido los datos correctos, procesamos. 
            $response = json_decode($res);
            $responseOut['error'] = 0;
            $responseOut['result'] = 'OK';
            $responseOut['msg'] = '';
            $responseOut['counter'] = (int) $response->requests;
        }
        return response()->json($responseOut, $this->getResponseHttpStatus());
    }

    /**
     * función pública que procesa la respuesta del chequeo de elementos en una blacklist
     *
     * 
     * @param json $res
     * @return array Respuesta 
     */
    private function _checkInputResponse($res)
    {
        $responseOut = ['error' => 0, 'result' => 'OK', 'msg' => 'Not in Robinson List', 'status' => $this->http_status, 'inRobinson' => 0, 'data' => []];
        if ($this->http_status == 200) {
            //Está en la robinson!!!
            $response = json_decode($res);
            $responseOut['inRobinson'] = 1;
            $responseOut['msg'] = 'Element in Robinson List';
            $responseOut['data']['sectors'] = $response->sectors;
        } elseif ($this->http_status != 404) {
            //La petición no se puede procesar. devolvemos KO y error
            $responseOut['error'] = 1;
            $responseOut['result'] = 'KO';

            $responseOut['msg'] = 'Request cannot be processed right now';
        }
        return $responseOut;
    }

    function checkInput2(Request $request)
    {
        $this->algorithm = config('apis.robinson.algorithm');

        echo json_encode($request);
    }

    /**
     * Función que devuelve el código HTTPStatus de la llamada a la api
     *
     * @return integer
     */
    private function getResponseHttpStatus()
    {
        $out = ($this->http_status == 404 ? $this->httpsStatusResponseList['NOTROBINSON'] : ($this->http_status != 200 ? $this->httpsStatusResponseList['ERROR'] : $this->httpsStatusResponseList['INROBINSON']));
        $this->____debug('vamos fuera');
        $this->____debug($out);
        return ($out);
    }

    /**
     * Función que accede a la BBDD y comprueba si ya ha sido chequeado, y si lo ha sido, devuelve el valor para ser chequeqdo de nuevo o no
     * Actualizamos el campo 
     *
     * @param string $email
     * @return mixed Boolean si no es encontrado, un array con la respuesta ya directamente para devolver
     */
    private function checkHistoricChecksEmail($email)
    {
        $data = DB::connection('api_robinson')->table('email_checks')->where('email', $email)->first();
        $this->____debug($data);


        if (empty($data))
            return false;
        $status_id = (int) $data->status_id;



        /**
         * Vemos la diferencia en días. Si sobrepasa la variable en el config, volvemos a chequear
         */
        $lastChecked = $data->checked_at;
        $date = Carbon::parse($lastChecked);
        $now = Carbon::now();
        $diffInDays = $date->diffInDays($now);
        $this->____debug('Difference in days');
        $this->____debug($diffInDays);

        $inRobinson = (int) $data->in_robinson;

        $status_id = ((int)$data->response_http_status == 403) ? 0 : $status_id ;

        /**
         * Si no lo hemos chequeado, o no es robinson y además han pasado días desde cuando hicimos la útlima comprobación, realizamos la comprobación de nuevo
         */
        $id = $data->id;
        if ($status_id <= 1 || ($inRobinson == 0 && $diffInDays > config('apis.robinson.diff_in_days_recheck'))) {
            $this->____debug('chequeamos');
            //Lo tenemos para chequear pero aún no lo hemos hecho, o, se chequeó hace tiempo y volvemos a chequear
            $res = $this->_checkInputResponse($this->checkItem('EMAIL', $email));
            //$id = $data->id;
            //Actualizamos el registro con los datos encontrados. 
            if ($this->http_status == 200) {
                $status_id_new = 2;
                $in_robinson_new = 1;
            } elseif ($this->http_status == 404) {
                $status_id_new = 2;
                $in_robinson_new = 0;
            } else {
                $status_id_new = 1;
                $in_robinson_new = 0;
            }
            DB::connection('api_robinson')->table('email_checks')->where('id', $id)->update([
                'status_id' => $status_id_new,
                'in_robinson' => $in_robinson_new,
                'response_http_status' => $this->http_status,
                'response_full' => $this->http_full_response,
                'ip_source' => $this->ip,
                'checked_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'lastaccess_at' => Carbon::now()->format('Y-m-d H:i:s')

            ]);

            return $res;
        }

        DB::connection('api_robinson')->table('email_checks')->where('id', $id)->update([
            'ip_source' => $this->ip,
            'lastaccess_at' => Carbon::now()->format('Y-m-d H:i:s')

        ]); 
        $in_robinson = $data->in_robinson;
        $this->http_status = $data->response_http_status;
        $response_full = $data->response_full;
        $this->____debug($response_full);
        $res = $this->_checkInputResponse($response_full);
        $this->____debug($res);
        return $res;
    }


    /**
     * Función que inserta en la tabla de log el chequeo realizado
     *
     * @param string $email email resultante
     * @param array $res objeto resultante
     * @return void
     */
    private function insertCheckEmail($email, $res)
    {
        $id = DB::connection('api_robinson')->table('email_checks')->insertGetId([
            'status_id' => 2,
            'email' => $email,
            'in_robinson' => $res['inRobinson'],
            'response_http_status' => $this->http_status,
            'response_full' => $this->http_full_response,
            'ip_source' => $this->ip,
            'checked_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::connection('api_robinson')->table('check_historical')->insert([
            'check_type_id' => 1,
            'item_id' => $id
        ]);
    }

    /**
     * Función principal que chequea si un Email está en la lista Robinson
     *
     * @param RobinsonEmailValidator $request Validator
     * @param string $email Emamil a validar
     * @return json
     */
    public function checkInputEmail(RobinsonEmailValidator $request, $email)
    {
        $this->algorithm = config('apis.robinson.algorithm');
        $this->ip = $request->ip();
        $this->check_type_current = 'EMAIL';

        $email = strtolower(trim($email));
        $res = $this->checkHistoricChecksEmail($email);


        if ($res === false) {
            $res = $this->_checkInputResponse($this->checkItem('EMAIL', $email));
            $this->____debug($res);
            $this->insertCheckEmail($email, $res);
        }


        return response()->json($res, $this->getResponseHttpStatus());
    }


    /**
     * Función que busca en históricos si se ha chequeado ya y por tanto, si volvemos a chequear o qué hacemos. 
     *
     * @param string $telephone teléfono
     * @return mixed Boolean o array del resultado
     */
    private function checkHistoricChecksTelephone($telephone)
    {
        $outputCheck = ['SMS' => false, 'CALL' => false, 'SMSRESP' => [], 'CALLRESP' => [], 'id' => 0];
        $data = DB::connection('api_robinson')->table('telephone_checks')->where('telephone', $telephone)->first();
        $this->____debug($data);


        if (empty($data)) {
            //o return false directamente.
            $outputCheck = ['SMS' => true, 'CALL' => true, 'SMSRESP' => [], 'CALLRESP' => [], 'id' => 0];
            return $outputCheck;
        }

        $status_id = (int) $data->status_id;
        $id = $data->id;
        $outputCheck['id'] = $id;


        /**
         * Vemos la diferencia en días. Si sobrepasa la variable en el config, volvemos a chequear
         */
        $lastChecked_call = $data->checked_call_at;
        $lastChecked_sms = $data->checked_sms_at;

        $inRobinson = boolval($data->in_robinson);
        $http_status_call = (int)$data->response_http_status_call;
        $http_status_sms = (int)$data->response_http_status_sms;
        $fullResponse_call = $data->response_full_call;
        $fullResponse_sms = $data->response_full_sms;
        if ($inRobinson) {
            //Si está en Robinson no tenems que hacer nada. Solo devolver los datos tal como están
            $inRobinson_call = $data->in_robinson_call;
            $inRobinson_sms = $data->in_robinson_sms;
            $outputCheck['SMS'] = !boolval($inRobinson_sms);
            $outputCheck['CALL'] = !boolval($inRobinson_call);

            $this->http_status = $http_status_call;
            $outputCheck['CALLRESP'] = $this->_checkInputResponse($fullResponse_call);
            $this->http_status = $http_status_sms;
            $outputCheck['SMSRESP'] = $this->_checkInputResponse($fullResponse_sms);


            DB::connection('api_robinson')->table('telephone_checks')->where('id', $id)->update([
                'lastaccess_at' => Carbon::now()->format('Y-m-d H:i:s')
    
            ]); 


            return $outputCheck;
        }

        /**
         * Si llegamos aquí es que está en la bbdd PERO no está en la blacklist. Teneemos que ver cuando ha sido la última vez de chequeo y si hay que chequear. 
         */

        $date = Carbon::parse($lastChecked_call);
        $now = Carbon::now();
        $diffInDaysCall = $date->diffInDays($now);

        $outputCheck['CALL'] = ($diffInDaysCall >  config('apis.robinson.diff_in_days_recheck')) || ($http_status_call == 403);
        if (!$outputCheck['CALL']) {
            $this->http_status = $http_status_call;
            $outputCheck['CALLRESP'] = $this->_checkInputResponse($fullResponse_call);
        }
        $outputCheck['SMS'] = true;
        if (!empty($lastChecked_sms)) {
            $date = Carbon::parse($lastChecked_sms);
            $now = Carbon::now();
            $diffInDaysSms = $date->diffInDays($now);
            $outputCheck['SMS'] = ($diffInDaysSms >  config('apis.robinson.diff_in_days_recheck'))  || ($http_status_sms == 403);
            if (!$outputCheck['SMS']) {
                $this->http_status = $http_status_sms;
                $outputCheck['SMSRESP'] = $this->_checkInputResponse($fullResponse_sms);
            }
        }
        if (!$outputCheck['CALL'] && !$outputCheck['SMS']) {
            DB::connection('api_robinson')->table('telephone_checks')->where('id', $id)->update([
                'lastaccess_at' => Carbon::now()->format('Y-m-d H:i:s')
    
            ]); 
        }
        return $outputCheck;
        $this->____debug('Difference in days');
        $this->____debug($diffInDaysCall);
    }

    /**
     * Función privada que inserta o actualiza la tabla telephone_checks
     *
     * @param string $type tipo de chequeo realizado CALL, SMS
     * @param string $telephone teléfono chequeado
     * @param array $data donde se encuentra la información
     * @param integer $id si es distinto de 0, id de la tabla telephone_checks
     * @return void
     */
    private function _insertUpdateTelephoneStatus($type, $telephone, $data, $id = 0)
    {
        //$this->http_full_response
        $postFix = strtolower(trim($type));
        $arrIns = [
            'status_id' => 2,
            'telephone' => $telephone,
            'in_robinson' => (int) ($data['data']['call']['inRobinson'] == 1 || $data['data']['sms']['inRobinson'] == 1),
            'ip_source'=>$this->ip


        ];
        if (empty($id)) {
            $arrIns['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
        }
        
        switch ($type) {
            case 'CALL':
                $arrIns['in_robinson_call'] = $data['data']['call']['inRobinson'];
                $arrIns['response_http_status_call'] = $data['data']['call']['status'];
                $arrIns['response_http_status_sms'] = $data['data']['sms']['status'];

                $arrIns['response_full_call'] = $this->http_full_response;
                $arrIns['checked_call_at'] = Carbon::now()->format('Y-m-d H:i:s');


                break;
            case 'SMS':
                $arrIns['in_robinson_sms'] = $data['data']['sms']['inRobinson'];
                $arrIns['response_http_status_sms'] = $data['data']['sms']['status'];
                $arrIns['response_full_sms'] = $this->http_full_response;
                $arrIns['checked_sms_at'] = Carbon::now()->format('Y-m-d H:i:s');

                break;
        }
        
        //DB::connection('api_robinson')->table('email_checks')->insertGetId([
        $check_type_id = ($type=='CALL') ? 3 : 2;
        if (empty($id)) {
            //Es insert
            $id = DB::connection('api_robinson')->table('telephone_checks')->insertGetId($arrIns);
            DB::connection('api_robinson')->table('check_historical')->insert([
                'check_type_id' => $check_type_id,
                'item_id' => $id
            ]);
            

        } else {
            //Es Update
            $arrIns['retrieved_at'] = Carbon::now()->format('Y-m-d H:i:s');
            DB::connection('api_robinson')->table('telephone_checks')->where('id',$id)->update($arrIns);
            DB::connection('api_robinson')->table('check_historical')->insert([
                'check_type_id' => $check_type_id,
                'item_id' => $id
            ]);
            
        }
        return $id;
    }

    /**
     * Función que chequea si un teléfono está en la lista robinson por 2 canales. 
     * El teléfono funciona distinto al email donde solo hay un chequeo. En este caso, hay 2 canales a chequear: llamadas y SMS.
     * Si el primero da false, se chequea el segundo ya que nosotros no tenemos aún una BlackList por canal (sí podemos tenerla por sectores)
     * Por orden de Prioridad:
     *      TELEPHONE-CALL
     *      TELEPHONE-SMS
     *
     * @param RobinsonTelephoneValidator $request Validator
     * @param string $telephone Teléfono
     * @return json
     */
    public function checkInputTelephone(RobinsonTelephoneValidator $request, $telephone)
    {
        $this->algorithm = config('apis.robinson.algorithm');
        $responseOut = ['error' => 0, 'result' => 'OK', 'msg' => 'Not in Robinson List', 'inRobinson' => 0, 'status' => 200, 'data' => [
            'call' => [
                'inRobinson' => 0,
                'status' => 0,
                'msg' => '',
                'sectors' => []
            ],
            'sms' => [
                'inRobinson' => 0,
                'status' => 0,
                'msg' => '',
                'sectors' => []
            ]
        ]];

       

        $telephone = trim($telephone);
        $telephoneInput = $telephone;
        
        /**
         * Si realizamos el chequeo de CALL primero, y es robinson, que lo sepamos antes de realizar el chequeo para SMS
         */
        $inRobinson = false;
        /**
         * Normalizamos el teléfono. Debe ser de la forma 0034NUMERO
         * LA VALIDAción existente puede recibir el número como: [+34,0034,34][6,7,8,9][8 digitos]
         */
        if (strlen($telephone) == 9) {
            $telephone = '0034' . $telephone;
        } elseif (strlen($telephone) > 9) {
            $telephone = '0034' . substr($telephone, -9);
        }
        $telephoneInput = substr($telephone,-9);
        $historicalCheck = $this->checkHistoricChecksTelephone($telephoneInput);

        
        /**
         * Id de la tabla telephone_checks. Primero se insertará la llamada de Call, y poseeteriormente se 
         */
        $id = $historicalCheck['id'];
        if ($historicalCheck['CALL'] && $historicalCheck['SMS']) {
            /**
             * REALIZAMOS el chequeo para call primero. 
             */
           
            $this->check_type_current = 'TELEPHONE-CALL';

            $this->____debug('Chequeamos 1 ' . $telephone);
            // dd($telephone);

            $responseCall = $this->_checkInputResponse($this->checkItem('TELEPHONE-CALL', $telephone));
           
            //dd($responseCall);
            // $responseCall = $this->_checkInputResponse($res);
            if ($responseCall['error'] == 1) {
                $responseOut['error'] = 1;
                $responseOut['result'] = 'KO';
                $responseOut['status'] = $responseCall['status'];

                $responseOut['msg'] = $responseCall['msg'];
                $responseOut['data']['call']['status'] = $responseCall['status'];
                $responseOut['data']['sms']['status'] = $this->httpsStatusResponseList['NOTPROCESSED'];
                $this->____debug('Salimos con Error en 1');
                $this->____debug($responseOut);
                return response()->json($responseOut, $this->getResponseHttpStatus());
            } elseif ($responseCall['status'] == 200) {
                /**
                 * el teléfono para Call está, por lo tanto no miramos SMS
                 */
                $responseOut['msg'] = $responseCall['msg'];
                $responseOut['inRobinson'] = 1;
                $responseOut['status'] = 200;
                $inRobinson = true;
                $responseOut['data']['call']['inRobinson'] = 1;
                $responseOut['data']['call']['status'] = $responseCall['status'];
                $responseOut['data']['call']['msg'] = $responseCall['msg'];
                $responseOut['data']['call']['sectors'] = $responseCall['data']['sectors'];
                $responseOut['data']['sms']['status'] = $this->httpsStatusResponseList['NOTPROCESSED'];
                $this->____debug('Salimos con Robinson en 1');
                $this->____debug($responseOut);
                $this->_insertUpdateTelephoneStatus('CALL', $telephoneInput, $responseOut, $id);
                
                return response()->json($responseOut, $this->getResponseHttpStatus());
            } else {
                /**
                 * No está en la lista robinson el chequeo del CALL 
                 * Chequeamos para SMS
                 */
                $responseOut['data']['call']['inRobinson'] = 0;
                $responseOut['data']['call']['status'] = $responseCall['status'];
                $responseOut['data']['call']['msg'] = $responseCall['msg'];
                $id = $this->_insertUpdateTelephoneStatus('CALL', $telephoneInput, $responseOut, $id);

                $this->____debug('Chequeamos 2');
            }
        } elseif (isset($historicalCheck['CALLRESP']['status'])) {
            
            if ($historicalCheck['CALLRESP']['status'] == 200 && $historicalCheck['CALLRESP']['error'] == 0) {
                $responseOut['msg'] = $historicalCheck['CALLRESP']['msg'];
                $responseOut['inRobinson'] = 1;
                $inRobinson = true;
                $responseOut['data']['call']['inRobinson'] = 1;
                $responseOut['data']['call']['status'] = $historicalCheck['CALLRESP']['status'];
                $responseOut['data']['call']['msg'] = $historicalCheck['CALLRESP']['msg'];
                $responseOut['data']['call']['sectors'] = $historicalCheck['CALLRESP']['data']['sectors'];
                $responseOut['data']['sms']['status'] = $this->httpsStatusResponseList['NOTPROCESSED'];
            }
            $responseOut['status'] = $historicalCheck['CALLRESP']['status'];
            
        }
        if (!$inRobinson && $historicalCheck['SMS']) {
          
            /**
             * ya hemos chequeado CALL y da erróneo, o no neceistábamos chequearlo
             */
            $this->check_type_current = 'TELEPHONE-SMS';

            $responseCall = $this->_checkInputResponse($this->checkItem('TELEPHONE-SMS', $telephone));
            if ($responseCall['error'] == 1) {
                $responseOut['error'] = 1;
                $responseOut['result'] = 'KO';
                $responseOut['status'] = $responseCall['status'];

                $responseOut['msg'] = $responseCall['msg'];
                $responseOut['data']['sms']['status'] = $responseCall['status'];
                $this->____debug('Salimos con Error en 2');
                $this->____debug($responseOut);
                return response()->json($responseOut, $this->getResponseHttpStatus());
            } elseif ($responseCall['status'] == 200) {
                /**
                 * el teléfono para Call está, por lo tanto no miramos SMS
                 */
                $responseOut['msg'] = $responseCall['msg'];
                $responseOut['inRobinson'] = 1;
                $responseOut['status'] = 200;

                $responseOut['data']['sms']['inRobinson'] = 1;
                $responseOut['data']['sms']['status'] = $responseCall['status'];
                $responseOut['data']['sms']['msg'] = $responseCall['msg'];
                $responseOut['data']['sms']['sectors'] = $responseCall['data']['sectors'];
                $this->____debug('Salimos con Robinson en 2');
                $this->____debug($responseOut);
                $this->_insertUpdateTelephoneStatus('SMS', $telephoneInput, $responseOut, $id);

                return response()->json($responseOut, $this->getResponseHttpStatus());
            } else {
                $responseOut['data']['sms']['status'] = $responseCall['status'];
                $responseOut['status'] = $responseCall['status'];
                $this->_insertUpdateTelephoneStatus('SMS', $telephoneInput, $responseOut, $id);
            }
        } elseif (isset($historicalCheck['SMSRESP']['status'])) {
            
            if ($historicalCheck['SMSRESP']['status'] == 200 && $historicalCheck['SMSRESP']['error'] == 0) {
                
                $responseOut['msg'] = $historicalCheck['SMSRESP']['msg'];
                $responseOut['inRobinson'] = 1;
                $inRobinson = true;
                $responseOut['data']['sms']['inRobinson'] = 1;
                
                $responseOut['data']['sms']['status'] = $historicalCheck['SMSRESP']['status'];
                
                $responseOut['data']['sms']['msg'] = $historicalCheck['SMSRESP']['msg'];
                $responseOut['data']['sms']['sectors'] = $historicalCheck['SMSRESP']['data']['sectors'];
               // $responseOut['data']['sms']['status'] = $this->httpsStatusResponseList['NOTPROCESSED'];
            }
            $responseOut['status'] = $historicalCheck['SMSRESP']['status'];
            
           
        }

        $this->____debug('Salimos con Robinson en general');
        $this->____debug($responseOut);
        return response()->json($responseOut, $responseOut['status']);
    }


    /**
     * Función que devuelve un array con la cabecera requerida para la comunicación
     *
     * @param string $digitalHash Si es de chequeo, hash que hay que añadir en la elaboración de la cabecera
     * @return array
     */
    private function getRequestHeader($digitalHash = '')
    {
        //date_default_timezone_set('Europe/Madrid');
        $canonicalSign = [];
        $stringToSign = [];
        $authorizationHeader = [];
        $headers = [];

        $date = date('Ymd');
        $dateHour = date('His');
        //$dateHour = '111009';

        $this->____debug($date.' -- '.$dateHour);
        $params = '';
        $enver = '';
        if ($this->___debug) {
            $date = '20150830';
            $dateHour = '123600';
            $params = 'Param1=value1&Param2=value2';
            $enver = '-test';
        }


        $canonicalSign[] = 'GET';
        $canonicalSign[] = config('apis.robinson' . $enver . '.address-' . strtolower($this->checkType)) . $digitalHash; //hash('sha256',$stringDigitalHash);
        $canonicalSign[] = $params;
        $canonicalSign[] = 'host:' . config('apis.robinson' . $enver . '.host');
        $canonicalSign[] = 'x-amz-date:' . $date . 'T' . $dateHour . 'Z';
        $canonicalSign[] = '';
        $canonicalSign[] = 'host;x-amz-date';
        $canonicalSign[] = hash($this->algorithm, '');
        $this->____debug($canonicalSign);
        $this->____debug($this->algorithm);

        $canonicalSignHash = hash($this->algorithm, implode("\n", $canonicalSign));
        $this->____debug('Cannonical Sign Hash');
        $this->____debug($canonicalSignHash);
        /**
         * STRING TO SIGN
         */
        $scope = $date . "/" . config('apis.robinson' . $enver . '.aws-region') . "/" . config('apis.robinson' . $enver . '.aws-service') . "/" . config('apis.robinson' . $enver . '.aws-request');
        $stringToSign[] = 'AWS4-HMAC-' . strtoupper($this->algorithm);
        $stringToSign[] = $date . 'T' . $dateHour . 'Z';
        $stringToSign[] = $scope;
        $stringToSign[] = $canonicalSignHash;
        $stringToSignHash = hash($this->algorithm, implode("\n", $stringToSign));
        $this->____debug($stringToSign);
        $this->____debug($stringToSignHash);
        /**
         * AuthorizationHeader SignCalculation
         */
        $this->____debug(config('apis.robinson' . $enver . '.api_secret'));
        $kSecret = 'AWS4' . config('apis.robinson' . $enver . '.api_secret');
        $kDate = hash_hmac($this->algorithm, $date, $kSecret, true);
        $kRegion = hash_hmac($this->algorithm, config('apis.robinson' . $enver . '.aws-region'), $kDate, true);
        $kService = hash_hmac($this->algorithm, config('apis.robinson' . $enver . '.aws-service'), $kRegion, true);
        $kSigning = hash_hmac($this->algorithm, config('apis.robinson' . $enver . '.aws-request'), $kService, true);
        $signature = hash_hmac($this->algorithm, implode("\n", $stringToSign), $kSigning);

        $authorizationHeader[] = 'Credential=' . config('apis.robinson' . $enver . '.api_key') . "/" . $scope;
        $authorizationHeader[] = 'SignedHeaders=host;x-amz-date';
        $authorizationHeader[] = 'Signature=' . $signature;
        $authorizationHeaderTxt = 'AWS4-HMAC-' . strtoupper($this->algorithm) . ' ' . implode(', ', $authorizationHeader);

        $headers['Authorization'] = $authorizationHeaderTxt;
        $headers['Host'] = config('apis.robinson' . $enver . '.host');
        $headers['X-Amz-Date'] = $date . 'T' . $dateHour . 'Z';


            $this->____debug($headers);
        //$this->____debug($authorizationHeaderTxt);
        //$this->____debug($signature);
        return $headers;
    }
    /**
     * Función privada que chequea si el email o teléfono son correctos o están en la lista robinson
     *
     * @param string $type enum('email','telephone')
     * @param string $item item a chequear
     * @return void
     */
    private function checkItem($type, $item)
    {

        switch ($type) {
            case 'EMAIL':
                $stringDigitalHash = '02';
                break;
            case 'TELEPHONE-SMS':
                $stringDigitalHash = '03';
                break;
            case 'TELEPHONE-CALL':
                $stringDigitalHash = '04';
                break;

            default:
                # code...
                $stringDigitalHash = '02';
                break;
        }

        //$date = '20190206';
        //$dateHour = '122726';
        $stringDigitalHash .= $item;
        $digitalHash = hash($this->algorithm, $stringDigitalHash);
        //$digitalHash = '998202202f5b7a2027d8acb1775419f7c9cb684f1889ff559a622b0efe9d0ba3';
        $this->____debug($digitalHash);
        $headers = $this->getRequestHeader($digitalHash);
        return $this->sendRequest($digitalHash, $headers);
        //return (hash($this->algorithm, implode("\n", $stringToSign)));
        // return $string;

    }

    /**
     * Funcón que manda la propia request
     *
     * @param string $type Tipo de Request. CHECKINLIST, REQUESTCOUNT
     * @param string $digitalHash Firma digital a incluir en la URL
     * @param array $headers Cabeceras a enviar
     * @return json
     */
    private function sendRequest($digitalHash, $headers)
    {
        $url = config('apis.robinson.host-protocol') . "://" . config('apis.robinson.host') . config('apis.robinson.address-' . strtolower($this->checkType)) . $digitalHash;
        //dd($url);
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url, ['headers' => $headers, 'debug' => $this->___debug, 'http_errors' => false,'timeout'=>0,'connect_timeout'=>0]);
        $this->http_status = $statusCode = $response->getStatusCode();
        $header = [];
        foreach ($response->getHeaders() as $name => $values) {
            $header[] = $name . ": " . implode(", ", $values);
        }

        $resp = (string) $response->getBody();
        $this->http_full_response = $resp;
        $this->____debug($statusCode);
        $this->____debug($header);
        $this->____debug($resp);
        unset($client);
        return $resp;
    }
}
