<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('client')->comment('Client Name');
            $table->string('description')->nullable()->comment('Big Description of the client');
            $table->string('ip_list')->nullable()->comment('Deprecated Field. List of IPs ONLY as information');
            $table->boolean('active')->default(true)->comment('If client is active or not');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });
        DB::table('clients')->insert([
            ['client'=>'CRM','description'=>'e-retail CRM App','ip_list'=>'176.58.125.192'],
            ['client'=>'GTP Captación','description'=>'e-retail Aplicación de Captación de Datos','ip_list'=>'85.159.213.228'],
            ['client'=>'DEV','description'=>'e-retail Devel Server','ip_list'=>'136.162.246.12'],
            ['client'=>'WS-Server','description'=>'e-retail WS Server Versión 1.','ip_list'=>'212.71.235.216'],
            ['client'=>'e-retail Offices','description'=>'e-retail Oficinas','ip_list'=>'62.97.126.66'],
        ]);
        if (env('APP_ENV')=='local') {
            DB::table('clients')->insert([
                ['client'=>'Luisfer Development','description'=>'Computer Mac of Development from Luisfer (TM)','ip_list'=>'176.58.125.192'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
