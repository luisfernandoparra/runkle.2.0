<?php
/*
 * Created on Fri Jul 19 2019
 *
 * Copyright (c) 2019 e-retail-data
 * 
 * @author LFP <lfparra@e-retailadvertising.com>
 * 
 * Fichero de configuración de distintos parámetros de las Apis
 * 
 * @date 2020.04.27
 *      Ampliamos a 30 días la variaqble diff_in_days_recheck
 * 
 * 
 */
return [
    'robinson'=> [
        'aws-region'=>'eu-west-1',
        'aws-service'=>'execute-api',
        'aws-request'=>'aws4_request',
        'host-protocol'=>'https',
        'host'=>'api.listarobinson.es',
        'address-checkinlist'=>'/v1/api/user/',
        'address-requestcount'=>'/v1/api/requests',
        'algorithm'=>'sha256',
        'diff_in_days_recheck'=>30,
        'api_key'=>env("ROBINSON_API_KEY",""),
        'api_secret'=>env("ROBINSON_API_SECRET",""),
        'urlCounter'=>"https://ws-server2.e-retaildata.com/api/robinson-list/request-count"

   ],
    'robinson-test'=> [
        'aws-region'=>'us-east-1',
        'aws-service'=>'service',
        'aws-request'=>'aws4_request',
        'host-protocol'=>'https',
        'host'=>'example.amazonaws.com',
        'address-checkinlist'=>'/api/user/',
        'address-requestcount'=>'/',
        'algorithm'=>'sha256',
        'diff_in_days_recheck'=>30,
        'api_key'=>env("ROBINSON_API_KEY",""),
        'api_secret'=>env("ROBINSON_API_SECRET",""),
        'urlCounter'=>"https://ws-server2.e-retaildata.com/api/robinson-list/request-count"
        

    ]
];



?>